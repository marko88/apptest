//
//  URLs.swift
//  TestApp
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation

enum APIRouteKeys: String {
    case Weather = "/weather"
    case Location = "/location"
}

struct URLs {
    
    static fileprivate let siteURL = "http://ios.quantox.tech"
    static fileprivate let apiV1 = "/api/v1"
    
    static let apiKey = "ffCnDu2QjRedD7TrUxQmZxEfvuc4fFHYcr73891P"
    static let language = (en: "en", sr: "sr")
    
    static func getRoute(for route: APIRouteKeys) -> String {
        
        return siteURL + apiV1 + route.rawValue
        
    }
    
}
