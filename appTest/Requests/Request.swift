//
//  Request.swift
//  TestApp
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class Request {
    
    // MARK: - Private Vars
    fileprivate var _handler: NetworkRequestHandler
    fileprivate var _header = [String:String]()
    fileprivate var _isWorking = false
    
    let url: String
    
    // MARK: - Initializers
    init(for urlRoute: APIRouteKeys, handler: NetworkRequestHandler) {
        url = URLs.getRoute(for: urlRoute)
        self._handler = handler
        
        _header["api-key"] = URLs.apiKey
        _header["language"] = URLs.language.en
    }
    
    // MARK: - Request method
    func makeRequest() {
        guard !_isWorking else {
            return
        }
        
        _isWorking = true
        showNetworkActivityIndicator()
        Alamofire.request(url, method: .get, parameters: nil, headers: _header)
            .response { responce in
                self._isWorking = false
                hideNetworkActivityIndicator()
                
                if let e = responce.error {
                    _consoleLog(for: .Networking, "Error requesting " + self.url + " | " + e.localizedDescription)
                    
                    self._handler.networkRequest(self, networkError: RequestError(responce.error as NSError?), responseStatus: .Failed, responceJSON: nil)
                }
            }
            .responseJSON { responce in
                switch responce.result {
                case .success:
                    _consoleLog(for: .Networking, String(describing: responce.response?.statusCode ?? 000) + " for " + self.url)
                    
                    if let data = responce.data, data.count > 0 {
                        self._handler.networkRequest(self, networkError: nil, responseStatus: .Sucess, responceJSON: JSON(data: data))
                    } else {
                        self._handler.networkRequest(self, networkError: nil, responseStatus: .Sucess, responceJSON: nil)
                    }
                case .failure(let error):
                    _consoleLog(for: .Networking, String(describing: responce.response?.statusCode) + " for " + self.url + " | " + error.localizedDescription)
                    
                    self._handler.networkRequest(self, networkError: nil, responseStatus: .Failed, responceJSON: nil)
                }
                
            }
    }
    
}
