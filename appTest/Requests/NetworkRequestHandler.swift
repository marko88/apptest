//
//  NetworkRequestHandler.swift
//  appTest
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation
import CFNetwork
import Alamofire
import SwiftyJSON

enum RequestError: Error {
    
    case NotConnectedToInternet
    case ErrorDomain
    case UserAuthenticationRequired
    case BadCredentials
    case Undefined
    
    var title: String {
        switch self {
        case .NotConnectedToInternet:
            return "No Network Conection"
        case .ErrorDomain:
            return "Wrong domain"
        case .UserAuthenticationRequired:
            return "Rquired credentials"
        case .BadCredentials:
            return "Bad credentials"
        default:
            return "Some error occurred"
        }
    }
    
    init(_ error: NSError?) {
        if let error = error {
            if error.domain == NSURLErrorDomain {
                self = .ErrorDomain
            } else if error.code == NSURLErrorNotConnectedToInternet {
                self = .NotConnectedToInternet
            } else if error.code == NSURLErrorUserAuthenticationRequired {
                self = .UserAuthenticationRequired
            } else if error.code == Int(CFNetworkErrors.cfErrorHTTPBadCredentials.rawValue) {
                self = .BadCredentials
            } else {
                self = .Undefined
            }
        } else {
            self = .Undefined
        }
    }
}

enum NetworkResponse {
    case Sucess
    case Failed
}

protocol NetworkRequestHandler: class {
    func networkRequest(_ networkRequest: Request, networkError error: RequestError?, responseStatus: NetworkResponse, responceJSON: JSON?) -> Void
}
