//
//  LocationsPresenter.swift
//  appTest
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreLocation

class LocationsPresenter: PresenterProtocol {
    
    // MARK: - Vars
    fileprivate weak var _viewUpdate: ViewUpdateProtocol?
    fileprivate var _locations = [LocationData]()
    fileprivate var _request: Request?
    fileprivate var _locationManager: LocationManager?
    fileprivate var _currentLocation: CLLocation? {
        didSet {
            self.calculateAllDistances()
            _viewUpdate?.dataUpdated(from: self)
        }
    }
    fileprivate var _locationBelgrade = CLLocation(
        latitude: CLLocationDegrees(44.787197),
        longitude: CLLocationDegrees(20.457273)
    )
    
    // MARK: - Getters
    var locations: [LocationData] { get { return _locations } }
    
    // MARK: - Initializers
    required init(view viewUpdate: ViewUpdateProtocol) {
        _viewUpdate = viewUpdate
        
        _request = Request(for: .Location, handler: self)
        _request?.makeRequest()
        
        _locationManager = LocationManager()
        _locationManager?.requestLocation { [weak self] currentLocation in
            self?._currentLocation = currentLocation
        }
    }
    
    // MARK: - Funcitons
    func refresh() {
        _request?.makeRequest()
    }
    
    func calculateAllDistances() {
        var myLocation: CLLocation?
        
        if let currentLocation = _currentLocation {
            myLocation = currentLocation
        } else {
            myLocation = _locationBelgrade
        }
        
        for location in locations {
            if let latitude = location.latitude,
                let longitude = location.longitude
            {
                let clLocation = CLLocation(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
                let distance = myLocation?.distance(from: clLocation)
                location.distance = distance
            }
        }
    }
}

// MARK: - NetworkRequestHandler
extension LocationsPresenter: NetworkRequestHandler {
    func networkRequest(_ networkRequest: Request, networkError error: RequestError?, responseStatus: NetworkResponse, responceJSON: JSON?) {
        
        guard error == nil else {
            _viewUpdate?.dataUpdated(from: self)
            _viewUpdate?.showAlert(for: .NetworkError)
            return
        }
        
        switch responseStatus {
        case .Sucess:
            if let listOfLocations = responceJSON?["data"].array {
                _locations.removeAll()
                
                for locationJSON in listOfLocations {
                    _locations.append(LocationData(fromJSON: locationJSON))
                }
                
                self.calculateAllDistances()
                _viewUpdate?.dataUpdated(from: self)
            }
        case .Failed:
            _viewUpdate?.dataUpdated(from: self)
            _viewUpdate?.showAlert(for: .ServerError)
        }
        
    }
}
