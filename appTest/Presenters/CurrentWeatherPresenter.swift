//
//  CurrentWeatherPresenter.swift
//  appTest
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation
import SwiftyJSON

class CurrentWeatherPresenter: PresenterProtocol {
    
    // MARK: - Vars
    fileprivate weak var _viewUpdate: ViewUpdateProtocol?
    fileprivate var _currentWeather: WeatherData?
    fileprivate var _request: Request?
    
    // MARK: - Getters
    var currentWeather: WeatherData? { get { return _currentWeather } }
    
    // MARK: - Initializers
    required init(view viewUpdate: ViewUpdateProtocol) {
        _viewUpdate = viewUpdate
        _request = Request(for: .Weather, handler: self)
        _request?.makeRequest()
    }
    
    // MARK: - Functios
    func refresh() {
        _request?.makeRequest()
    }
}

// MARK: - NetworkRequestHandler
extension CurrentWeatherPresenter: NetworkRequestHandler {
    func networkRequest(_ networkRequest: Request, networkError error: RequestError?, responseStatus: NetworkResponse, responceJSON: JSON?) {
        
        guard error == nil else {
            _viewUpdate?.dataUpdated(from: self)
            _viewUpdate?.showAlert(for: .NetworkError)
            return
        }
        
        switch responseStatus {
        case .Sucess:
            if let data = responceJSON?["data"] {
                _currentWeather = WeatherData(fromJSON: data)
                _viewUpdate?.dataUpdated(from: self)
            }
        case .Failed:
            _viewUpdate?.dataUpdated(from: self)
            _viewUpdate?.showAlert(for: .ServerError)
        }
        
    }
}
