//
//  PresenterProtocol.swift
//  appTest
//
//  Created by Marko on 9/17/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation

protocol PresenterProtocol: class {
    init(view: ViewUpdateProtocol)
    func refresh() -> Void
}
