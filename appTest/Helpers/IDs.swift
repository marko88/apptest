//
//  IDs.swift
//  appTest
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation

enum StoryboardIDs: String {
    case RootVC = "RootVC"
    case LocationsVC = "LocationsVC"
    case CurrentWeatherVC = "CurrentWeatherVC"
    case WeatherForecastCollectionViewCell = "WeatherForecastCollectionViewCell"
    case MapVC = "MapVC"
}

enum CellIDs: String {
    case LocationTableViewCell = "LocationTableViewCell"
}

enum SegueIDs: String {
    case ShowMapFromLocation = "ShowMapFromLocation"
}


