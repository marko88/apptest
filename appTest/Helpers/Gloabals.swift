//
//  Gloabals.swift
//  TestApp
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit

enum ConsoleMessageType: String {
    case Initilizers = "Initilizers"
    case LifeCycles = "LifeCycles"
    case CoreData = "CoreData"
    case Networking = "Networking"
    case Parsing = "Parsing"
    case Custom = "Custom"
    case Warnings = "Warnings"
}

func _consoleLog(for type: ConsoleMessageType, _ item: String) {
    print(type.rawValue + ":    " + item)
}

class AppColors {
    static let IconsYellow = SWColor(hexString: "#FFD300")
    static let StrokeBlue = SWColor(hexString: "#63C4FF")
    static let NavigationBlue = SWColor(hexString: "#003F60")
    static let DistanceLabel = SWColor(hexString: "#8E8E93")
}

func showNetworkActivityIndicator(){
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
}

func hideNetworkActivityIndicator(){
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
}
