//
//  Extensions.swift
//  appTest
//
//  Created by Marko on 9/17/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit

extension UIScrollView {
    func setUpRefreshWithBackground(background: UIColor, tintColor: UIColor, attributedTitle: NSAttributedString, actionSelector: Selector, forTarget target: AnyObject) -> UIRefreshControl {
        let refCon = UIRefreshControl()
        
        refCon.backgroundColor = background
        refCon.tintColor = tintColor
        refCon.attributedTitle = attributedTitle
        refCon.addTarget(target, action: actionSelector, for: .valueChanged)
        
        self.addSubview(refCon)
        
        return refCon
    }
}
