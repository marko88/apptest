//
//  LocationTableViewCell.swift
//  appTest
//
//  Created by Marko on 9/17/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit
import Haneke

class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var locationDistanceLabel: UILabel!
    @IBOutlet weak var pickerIconImage: UIImageView!
    
    var locationData: LocationData?
    
    var imageURL: String? {
        didSet {
            if let imageURL = imageURL {
                locationImage.hnk_setImage(from: URL(string: imageURL))
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        locationImage.layer.borderWidth = 2
        locationImage.layer.masksToBounds = false
        locationImage.layer.borderColor = AppColors.StrokeBlue?.cgColor
        locationImage.layer.cornerRadius = locationImage.frame.height / 2.4
        locationImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
