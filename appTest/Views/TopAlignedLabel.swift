//
//  TopAlignedLabel.swift
//  appTest
//
//  Created by Marko on 9/17/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit

@IBDesignable
class TopAlignedLabel: UILabel {
    
    // MARK: Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func drawText(in rect: CGRect) {
        
        guard text != nil else {
            return super.drawText(in: rect)
        }
        
        let height = self.sizeThatFits(rect.size).height
        let y = rect.origin.y
        super.drawText(in: CGRect(x: 0, y: y, width: rect.width, height: height))
    }
}
