//
//  WeatherForecastCollectionViewCell.swift
//  appTest
//
//  Created by Marko on 9/17/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit

class WeatherForecastCollectionViewCell: UICollectionViewCell {

    // MARK: - Outlets
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherDescLabel: TopAlignedLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        weatherDescLabel.textColor = AppColors.DistanceLabel
    }

}
