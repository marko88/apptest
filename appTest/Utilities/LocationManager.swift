//
//  LocationManager.swift
//  appTest
//
//  Created by Marko on 9/18/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    // MARK: - Vars
    let manager = CLLocationManager()
    var timer: Timer?
    let reverseGeocoder = CLGeocoder()
    fileprivate var completionHandler: ((CLLocation?) -> Void)?
    
    // MARK: Location Vars
    var currentLocation: CLLocation?
    var currentLocSet: Bool {
        if let _ = currentLocation{
            return true
        }
        
        return false
    }
    
    // MARK: Premissions
    var generalLocationServices: Bool {
        return CLLocationManager.locationServicesEnabled()
    }
    
    var whenInUsePer: Bool {
        return  (CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
    }
    
    var locationServicesOK: Bool {
        return ( generalLocationServices && whenInUsePer )
    }
    
    func updateLocation() {
        manager.requestLocation()
    }
    
    // MARK: - Lifecycles
    override init() {
        super.init()
        
        manager.delegate = self
        manager.distanceFilter = kCLDistanceFilterNone
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        
        //update location every 20 minutes
        timer = Timer(timeInterval: 20 * 60, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
    }
    
    deinit {
        timer?.invalidate()
    }
    
    // MARK: - Method for setting callback function
    @discardableResult
    func requestLocation(completionHandler: @escaping (CLLocation?) -> Void) -> Self {
        self.completionHandler = completionHandler
        manager.requestLocation()
        
        return self
    }
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if locationServicesOK {
            manager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.currentLocation = nil
        manager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations.first
        self.completionHandler?(self.currentLocation)
    }
}
