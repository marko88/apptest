//
//  AlertViewProtocol.swift
//  appTest
//
//  Created by Marko on 9/17/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation

enum AlertType {
    case NetworkError
    case ServerError
}

protocol AlertViewProtocol: class {
    func showAlert(for: AlertType) -> Void
}
