//
//  RootVC.swift
//  TestApp
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit

class RootVC: UITabBarController {

    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()

        tabBar.tintColor = AppColors.NavigationBlue
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
