//
//  NavigationVC.swift
//  appTest
//
//  Created by Marko on 9/17/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit

class NavigationVC: UINavigationController {

    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        navigationBar.titleTextAttributes = titleTextAttributes
        navigationBar.isTranslucent = false
        navigationBar.tintColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
