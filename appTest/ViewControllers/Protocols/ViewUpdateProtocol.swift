//
//  ViewUpdateProtocol.swift
//  appTest
//
//  Created by Marko on 9/17/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation

protocol ViewUpdateProtocol: class {
    func dataUpdated(from: PresenterProtocol) -> Void
    func showAlert(for: AlertType) -> Void
}
