//
//  AlertViewProtocol.swift
//  appTest
//
//  Created by Marko on 9/17/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit

enum AlertType {
    case NetworkError
    case ServerError
}

protocol AlertViewProtocol {
    func showAlert(for: AlertType) -> Void
}

extension AlertViewProtocol where Self: UIViewController {
    func showAlert(for alertType: AlertType) {
        switch alertType {
        case .NetworkError:
            createAndPresentAlert(
                title: "Network Error",
                message: "Please check your internet connection."
            )
        case .ServerError:
            createAndPresentAlert(
                title: "Server Error",
                message: "Some error occurred. Please try again later."
            )
        }
    }
    
    // MARK: - AlertController method
    func createAndPresentAlert(title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
