//
//  LocationsVC.swift
//  TestApp
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit

class LocationsVC: UIViewController, AlertViewProtocol {
    
    var presenter: LocationsPresenter?
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = LocationsPresenter(view: self)
        tableView.tableFooterView = UIView()
        
        if let navColor = AppColors.NavigationBlue {
            self.tableView.refreshControl = self.tableView.setUpRefreshWithBackground(
                background: UIColor.white,
                tintColor: navColor,
                attributedTitle: NSAttributedString(
                    string: "Refreshing list",
                    attributes: [NSForegroundColorAttributeName:navColor]
                ),
                actionSelector: #selector(refreshData),
                forTarget: self
            )
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Methods
    func refreshData() {
        self.presenter?.refresh()
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        guard let cell = sender.view?.superview?.superview?.superview as? LocationTableViewCell else {
            return
        }
        
        if let mapVC = self.storyboard?.instantiateViewController(withIdentifier: StoryboardIDs.MapVC.rawValue) as? MapVC {
            mapVC.locationData = cell.locationData
            self.navigationController?.pushViewController(mapVC, animated: true)
        }
    }
    
}

// MARK: - ViewUpdateProtocol
extension LocationsVC: ViewUpdateProtocol {
    func dataUpdated(from presenter: PresenterProtocol) {
        self.tableView.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension LocationsVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter?.locations.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.LocationTableViewCell.rawValue) as! LocationTableViewCell
        cell.selectionStyle = .none
        
        //add gesture for location pikerIcon tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        cell.isUserInteractionEnabled = true
        cell.pickerIconImage.isUserInteractionEnabled = true
        cell.pickerIconImage.addGestureRecognizer(tap)
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension LocationsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? LocationTableViewCell {
            let location = self.presenter?.locations[indexPath.row]
            cell.locationData = location
            cell.locationNameLabel.text = location?.name
            cell.locationDistanceLabel.textColor = AppColors.DistanceLabel
            cell.locationImage.image = nil
            cell.imageURL = location?.thumbURL
            
            var distanceDouble: Double = 0.0
            if let distance = location?.distance {
                distanceDouble = distance/1000
                distanceDouble = Double(round(10*distanceDouble)/10) // rounding to one decimal
            }
            
            cell.locationDistanceLabel.text = String(describing: distanceDouble) + " km"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}
