//
//  CurrentWeatherVC.swift
//  TestApp
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit

class CurrentWeatherVC: UIViewController, AlertViewProtocol {
    
    var presenter: CurrentWeatherPresenter?
    
    // MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var currentWeatherImage: UIImageView!
    @IBOutlet weak var currentWeatherTemperatureLabel: BottomAlignedLabel!
    @IBOutlet weak var currentWeatherDescriptionLabel: TopAlignedLabel!
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = CurrentWeatherPresenter(view: self)
        
        currentWeatherDescriptionLabel.textColor = AppColors.DistanceLabel
        
        if let navColor = AppColors.NavigationBlue {
            self.scrollView.refreshControl = self.scrollView.setUpRefreshWithBackground(
                background: UIColor.white,
                tintColor: navColor,
                attributedTitle: NSAttributedString(
                    string: "Refreshing weather",
                    attributes: [NSForegroundColorAttributeName:navColor]
                ),
                actionSelector: #selector(refreshData),
                forTarget: self
            )
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Methods
    func refreshData() {
        self.presenter?.refresh()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: - ViewUpdateProtocol
extension CurrentWeatherVC: ViewUpdateProtocol {
    func dataUpdated(from presenter: PresenterProtocol) {
        self.scrollView.refreshControl?.endRefreshing()
        
        var currentTemp: String = "--"
        var currentDesc: String = "--"
        
        if let currentTempFloat = self.presenter?.currentWeather?.currentC?.rounded() {
            currentTemp = String(Int(currentTempFloat))
        }
        
        if let currentTempDesc = self.presenter?.currentWeather?.currentWeather {
            currentDesc = currentTempDesc
        }
        
        
        currentWeatherTemperatureLabel.text = currentTemp + "\u{00B0}C"
        currentWeatherDescriptionLabel.text = currentDesc
        
        if let image = self.presenter?.currentWeather?.currentIcon {
            currentWeatherImage.image = UIImage(named: image)
        }
        
        self.collectionView.reloadData()
    }
}

// MARK: - UICollectionViewDataSource
extension CurrentWeatherVC: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellNib = UINib(nibName: StoryboardIDs.WeatherForecastCollectionViewCell.rawValue, bundle: nil)
        
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: StoryboardIDs.WeatherForecastCollectionViewCell.rawValue)
        
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: StoryboardIDs.WeatherForecastCollectionViewCell.rawValue, for: indexPath) as? WeatherForecastCollectionViewCell
        
        return cell ?? UICollectionViewCell()
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}

// MARK: - UICollectionViewDelegate
extension CurrentWeatherVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? WeatherForecastCollectionViewCell {
            
            var minTemp: String = "--"
            var maxTemp: String = "--"
            var desc: String = "----"
            var day: String = "----"
            var image: String?
            
            switch indexPath.row {
            case 0:
                if let minTempFloat = self.presenter?.currentWeather?.firstLowC {
                    minTemp = String(minTempFloat)
                }
                
                if let maxTempFloat = self.presenter?.currentWeather?.firstHighC {
                    maxTemp = String(maxTempFloat)
                }
                
                day = self.presenter?.currentWeather?.firstDay ?? "----"
                desc = self.presenter?.currentWeather?.firstWeather ?? "----"
                image = self.presenter?.currentWeather?.firstIcon
            case 1:
                if let minTempFloat = self.presenter?.currentWeather?.secondLowC {
                    minTemp = String(minTempFloat)
                }
                
                if let maxTempFloat = self.presenter?.currentWeather?.secondHighC {
                    maxTemp = String(maxTempFloat)
                }
                
                day = self.presenter?.currentWeather?.secondDay ?? "----"
                desc = self.presenter?.currentWeather?.secondWeather ?? "----"
                image = self.presenter?.currentWeather?.secondIcon
            default:
                break
            }
            
            cell.dayLabel.text = day
            cell.temperatureLabel.text = "\(minTemp)/\(maxTemp)\u{00B0}C"
            cell.weatherDescLabel.text = desc
            if let image = image {
                cell.imageView.image = UIImage(named: image)
            }
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CurrentWeatherVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.collectionView.frame.width
        let height = self.collectionView.frame.height
        
        return CGSize(width: width * 0.5, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
}

