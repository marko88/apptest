//
//  MapVC.swift
//  appTest
//
//  Created by Marko on 9/18/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController, MKMapViewDelegate {

    // MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: - Vars
    var locationData: LocationData?
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let location = locationData,
            let latitude = location.latitude,
            let longitude = location.longitude
        {
            self.mapView.setRegion(
                MKCoordinateRegion(
                    center: CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude)),
                    span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)),
                animated: true
            )
            
            let pinLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(latitude), CLLocationDegrees(longitude))
            let objectAnnotation = MKPointAnnotation()
            objectAnnotation.coordinate = pinLocation
            objectAnnotation.title = location.town
            self.mapView.addAnnotation(objectAnnotation)
        }
        
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "Identifier")
        
        annotationView.canShowCallout = true
        annotationView.image = UIImage(named: "ic_pin_route_2")
        
        return annotationView
    }
}
