//
//  WeatherData.swift
//  TestApp
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation
import SwiftyJSON

class WeatherData {
    
    // MARK: - Vars
    fileprivate var _id: Int?
    fileprivate var _lang: String?
    fileprivate var _current_weather: String?
    fileprivate var _current_icon: String?
    fileprivate var _current_f: Float?
    fileprivate var _current_c: Float?
    fileprivate var _first_day: String?
    fileprivate var _second_day: String?
    fileprivate var _first_weather: String?
    fileprivate var _second_weather: String?
    fileprivate var _first_icon: String?
    fileprivate var _second_icon: String?
    fileprivate var _first_low_c: Int?
    fileprivate var _first_low_f: Int?
    fileprivate var _first_high_c: Int?
    fileprivate var _first_high_f: Int?
    fileprivate var _second_low_c: Int?
    fileprivate var _second_low_f: Int?
    fileprivate var _second_high_c: Int?
    fileprivate var _second_high_f: Int?
    fileprivate var _created_at: String?
    fileprivate var _updated_at: String?
    
    // MARK: - Getters
    var id: Int? { get { return _id } }
    var lang: String? { get { return _lang } }
    var currentWeather: String? { get { return _current_weather } }
    var currentIcon: String? { get { return _current_icon } }
    var currentF: Float? { get { return _current_f } }
    var currentC: Float? { get { return _current_c } }
    var firstDay: String? { get { return _first_day } }
    var secondDay: String? { get { return _second_day } }
    var firstWeather: String? { get { return _first_weather } }
    var secondWeather: String? { get { return _second_weather } }
    var firstIcon: String? { get { return _first_icon } }
    var secondIcon: String? { get { return _second_icon } }
    var firstLowC: Int? { get { return _first_low_c } }
    var firstLowF: Int? { get { return _first_low_f } }
    var firstHighC: Int? { get { return _first_high_c } }
    var firstHighF: Int? { get { return _first_high_f } }
    var secondLowC: Int? { get { return _second_low_c } }
    var secondLowF: Int? { get { return _second_low_f } }
    var secondHighC: Int? { get { return _second_high_c } }
    var secondHighF: Int? { get { return _second_high_f } }
    var createdAt: String? { get { return _created_at } }
    var updatedAt: String? { get { return _updated_at } }
    
    // MARK: - Initializers
    init(fromJSON json: JSON) {
        _id = json["id"].int
        _lang = json["lang"].string
        _current_weather = json["current_weather"].string
        _current_icon = json["current_icon"].string
        _current_f = json["current_f"].float
        _current_c = json["current_c"].float
        _first_day = json["first_day"].string
        _second_day = json["second_day"].string
        _first_weather = json["first_weather"].string
        _second_weather = json["second_weather"].string
        _first_icon = json["first_icon"].string
        _second_icon = json["second_icon"].string
        _first_low_c = json["first_low_c"].int
        _first_low_f = json["first_low_f"].int
        _first_high_c = json["first_high_c"].int
        _first_high_f = json["first_high_f"].int
        _second_low_c = json["second_low_c"].int
        _second_low_f = json["second_low_f"].int
        _second_high_c = json["second_high_c"].int
        _second_high_f = json["second_high_f"].int
        _created_at = json["created_at"].string
        _updated_at = json["updated_at"].string
    }
}
