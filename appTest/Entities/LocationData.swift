//
//  LocationData.swift
//  TestApp
//
//  Created by Marko on 9/16/17.
//  Copyright © 2017 Marko. All rights reserved.
//

import Foundation
import SwiftyJSON

class LocationData {
    
    // MARK: - Vars
    fileprivate var _id: Int?
    fileprivate var _name: String?
    fileprivate var _description: String?
    fileprivate var _town: String?
    fileprivate var _address: String?
    fileprivate var _postal: Int?
    fileprivate var _photo_url: String?
    fileprivate var _photo_size: Int?
    fileprivate var _thumb_url: String?
    fileprivate var _thumb_size: Int?
    fileprivate var _latitude: Float?
    fileprivate var _longitude: Float?
    fileprivate var _created_at: String?
    fileprivate var _updated_at: String?
    
    // MARK: - Custom vars
    var distance: Double?
    
    // MARK: - Getters
    var id: Int? { get { return _id } }
    var name: String? { get { return _name } }
    var description: String? { get { return _description } }
    var town: String? { get { return _town } }
    var address: String? { get { return _address } }
    var postal: Int? { get { return _postal } }
    var photoURL: String? { get { return _photo_url } }
    var photoSize: Int? { get { return _photo_size } }
    var thumbURL: String? { get { return _thumb_url } }
    var thumbSize: Int? { get { return _thumb_size } }
    var latitude: Float? { get { return _latitude } }
    var longitude: Float? { get { return _longitude } }
    var createdAt: String? { get { return _created_at } }
    var updatedAt: String? { get { return _updated_at } }
    
    // MARK: - Initializers
    init(fromJSON json: JSON) {
        _id = json["id"].int
        _name = json["name"].string
        _description = json["description"].string
        _town = json["town"].string
        _address = json["address"].string
        _postal = json["postal"].int
        _photo_url = json["photo_url"].string
        _photo_size = json["photo_size"].int
        _thumb_url = json["thumb_url"].string
        _thumb_size = json["thumb_size"].int
        _latitude = json["latitude"].float
        _longitude = json["longitude"].float
        _created_at = json["created_at"].string
        _updated_at = json["updated_at"].string
    }
    
}
